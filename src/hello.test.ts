import { describe, expect, it } from "vitest"

describe("Markov Chain", () => {
  it("should return empty object when passing 1 word", () => {
    expect(markovChainAnalyse("les")).toEqual({ les: {} })
    expect(markovChainAnalyse("homme")).toEqual({ homme: {} })
  })

  it("should return empty object when passing empty string", () => {
    expect(markovChainAnalyse("")).toEqual({})
  })

  it("should return statics for 2 words", () => {
    expect(markovChainAnalyse("les hommes")).toEqual({
      les: { hommes: 100 },
      hommes: {},
    })
    expect(markovChainAnalyse("hommes les")).toEqual({
      hommes: { les: 100 },
      les: {},
    })
  })

  it("should return empty object when passing one word and a space", () => {
    expect(markovChainAnalyse("les ")).toEqual({ les: {} })
  })

  it("should return empty object when passing one word and a space", () => {
    expect(markovChainAnalyse("les       mots")).toEqual({
      les: { mots: 100 },
      mots: {},
    })
  })

  it("should return empty object when passing one word and a space", () => {
    expect(markovChainAnalyse("les hommes libres")).toEqual({
      les: { hommes: 100 },
      hommes: { libres: 100 },
      libres: {},
    })
  })

  it("should return statics for 4 words", () => {
    expect(markovChainAnalyse("les hommes libres peuvent")).toEqual({
      les: { hommes: 100 },
      hommes: { libres: 100 },
      libres: { peuvent: 100 },
      peuvent: {},
    })
  })
})

type StatisticsType = Record<string, Record<string, number>>

function markovChainAnalyse(trainingString: string): StatisticsType {
  const words = trainingString.split(/\s+/)
  let result: StatisticsType = {}
  for (let index = 0; index < words.length; index++) {
    const word = words[index]
    if (word) {
      newFunction(words, index, result, word)
    }
  }

  return result
}
function newFunction(
  words: string[],
  index: number,
  result: StatisticsType,
  word: string
) {
  const nextWord = words[index + 1]
  if (nextWord) result[word] = { [nextWord]: 100 }
  else result[word] = {}
}
